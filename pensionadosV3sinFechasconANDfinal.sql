with previsional as (select afi_hash64, min(pagfe3) fec_previsional, ifnull(sum(pagv01),0) suma_adicional
from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PAGAR2 p2
where p2.pagcon = '51'
group by afi_hash64
union all 
select b.afi_hash64 ID,min(movfe7) FEC_PREVISIONAL, ifnull(sum(movcot), 0) VAL_PREVISIONAL 
from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_MOVARC a inner join 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_AFIARC b
on a.movco4 = b.afinum
where movco1= '07' and movtip='31' and movest <> 'R'  
GROUP BY b.afi_hash64)
,sustituciones as (select a.afi_hash64 ident, ifnull(b.pagcon, '') susti 
from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PENARC a left outer join 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PAGAR2 b
on a.afi_hash64 = b.afi_hash64 and b.pagcon = '26')
select a.penti1
,a.pentip, a.pensol, c.solbp_nrosol, c.solbp_tipsol, c.solbp_codcaumod, e.susti 
,a.penest, has.primerNombre, has.segundoNombre, has.primerApellido, has.segundoApellido , has.tipoIdAfiliado, has.idAfiliado, ben.BENNU7, ben.BENTI6
,b.benca1, ben.BENPR6, ben.BENS02, ben.BENPR7, ben.BENS03
,b.bens01, benti7, b.bensu2  
,(select sum(bonv08) from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_BONAR4 where afi_hash64 = c.afi_hash64 and bones4 not in('acr','anu')) bono
,(select max(bonf39) from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_BONAR4 where afi_hash64 = c.afi_hash64 and bones4 not in('acr','anu')) fr_bono
,a.penf04, a.penfe1, d.datso_inddevafp
from notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PENARC a 
inner join notional-radio-302217.DatalakeAnalitica.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 has on 
has.afi_hash64 = a.afi_hash64
inner join afiliados-pensionados-prote.afiliados_pensionados.tabla_fed_Apolo ap on ap.tipoIdAfiliado = has.tipoIdAfiliado and ap.idAfiliado = has.idAfiliado
left outer join notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_BENAR3 b on a.afi_hash64 = b.afi_hash64 and a.pencon = b.bencon
left join notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_BENAR3_z51 ben on ben.ben_hash64=b.ben_hash64 and ben.afi_hash64= b.afi_hash64 

left join notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_SOLBP c on a.afi_hash64 = c.afi_hash64 and a.pensol = c.solbp_nrosol
left join notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_DATSO d on c.solbp_tipsol = d.solbp_tipsol and c.solbp_nrosol = d.solbp_nrosol and d.datso_progen = 'PEN'
left join sustituciones e on a.afi_hash64 = e.ident 

where a.penest <> 'rev' and b.benest <> 'edp' 
AND 
a.pencon = (SELECT MAX(M.PENCON)  
FROM `notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PENARC` M WHERE a.afi_hash64=M.afi_hash64 GROUP BY M.afi_hash64)
