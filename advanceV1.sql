SELECT B.SOLCD_ID ID_SOLICITUD, D.tipoIdAfiliado, D.idAfiliado,
json_value(HIADS_ANALISIS, '$.analisis.historiaLaboral.infoSupraBonal.bonos.0.totalSemanas') AS SEMANAS_BONO,
json_value(HIADS_ANALISIS, '$.analisis.historiaLaboral.semanasProteccion') AS SEMANAS_PROTECCION,
json_value(HIADS_ANALISIS, '$.analisis.historiaLaboral.semanasOtrasAFP') AS SEMANAS_OTRAS_AFP,
json_value(HIADS_ANALISIS, '$.analisis.historiaLaboral.totalSemanasCotizadas') AS TOTAL_SEMANAS
 FROM proteccion_co_zcarga.ORACLE_ADVANALISISCXN1_TADV_HISTORIAL_ANALISIS A, 
proteccion_co_zcarga.ORACLE_ADVANALISISCXN1_TADV_SOL_PENSION_VEJEZ B,
proteccion_co_zcarga.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 D
WHERE A.HIACD_SOLICITUD_ID=B.SOLCD_ID
AND A.HIANU_VERSION = (SELECT MAX(C.HIANU_VERSION) FROM proteccion_co_zcarga.ORACLE_ADVANALISISCXN1_TADV_HISTORIAL_ANALISIS C
WHERE C.HIACD_SOLICITUD_ID=A.HIACD_SOLICITUD_ID)
AND D.AFI_HASH64=B.AFI_HASH64
UNION ALL 
SELECT B.HIACD_ID_ANALISIS, D.tipoIdAfiliado, D.idAfiliado, '0',
/*json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.bonos.0.totalSemanas') AS SEMANAS_BONO,*/
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.semanasProteccion') AS SEMANAS_PROTECCION,
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.semanasOtrasAFP') AS SEMANAS_OTRAS_AFP,
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.totalSemanasCotizadas') AS TOTAL_SEMANAS
FROM proteccion_co_zcarga.ORACLE_ADVANALISISSBVCXN1_TADV_ANALISIS A, 
proteccion_co_zcarga.ORACLE_ADVANALISISSBVCXN1_TADV_HISTORIAL_ANALISIS B,
proteccion_co_zcarga.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 D
WHERE A.ASBVCD_ID = B.HIACD_ID_ANALISIS
AND B.HIANU_VERSION = (SELECT MAX(C.HIANU_VERSION) FROM proteccion_co_zcarga.ORACLE_ADVANALISISSBVCXN1_TADV_HISTORIAL_ANALISIS C
WHERE C.HIACD_ID_ANALISIS=B.HIACD_ID_ANALISIS)
AND D.AFI_HASH64=A.AFI_HASH64
UNION ALL 
SELECT B.HIACD_ID, D.tipoIdAfiliado, D.idAfiliado, '0',
/*json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.bonos.0.totalSemanas') AS SEMANAS_BONO,*/
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.semanasProteccion') AS SEMANAS_PROTECCION,
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.semanasOtrasAFP') AS SEMANAS_OTRAS_AFP,
json_value(B.HIADS_ANALISIS, '$solicitud.afiliado.historiaLaboral.totalSemanasCotizadas') AS TOTAL_SEMANAS
FROM proteccion_co_zcarga.ORACLE_ADVANALISISINVCXN1_TADV_ANALISIS A, 
proteccion_co_zcarga.ORACLE_ADVANALISISINVCXN1_TADV_HISTORIAL_ANALISIS B,
proteccion_co_zcarga.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 D
WHERE A.AINVCO_ID = B.HIACD_ID
AND B.HIANU_VERSION = (SELECT MAX(C.HIANU_VERSION) FROM proteccion_co_zcarga.ORACLE_ADVANALISISINVCXN1_TADV_HISTORIAL_ANALISIS C
WHERE C.HIACD_ID=B.HIACD_ID)
AND D.AFI_HASH64=A.AFI_HASH64
