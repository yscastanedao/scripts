-- =============================================
-- Author:		  Proteccion\DVASCO
-- Create date:   17-Dic-2020
-- Description:	  Extracci�n datos b�sicos de Apolo para Calculos y Recalculos

-- =============================================

--Datos b�sicos Apolo, mesadas y CAI
with Datos_Basicos AS(
select a.skPersona, a.tipoIdAfiliado, a.idAfiliado, a.nombreCompletoAfiliado
,a.primerNombre, a.segundoNombre, a.primerApellido, a.segundoApellido
,a.descSexo, a.dtmFechaNacimiento 
,l.idEstadoAfiliado
from BIProteccionDW.APOLO.vDimAfiliados a inner join BIProteccionDW.APOLO.vAuxLineasNegocioAfiliados l
on a.skPersona = l.fkPersona and l.idLineaNegocio = 'PO' and l.idEstadoAfiliado in('PEN','RES')
--where a.tipoIdAfiliado = 'CC' and a.idAfiliado IN( '0075456', '21658782', '8348515')
), CAI AS (select v.tipoIdAfiliado, v.idAfiliado, sum(v.valorFondoPesos) as valorFondoPesos 
from BIProteccionDW.DW.FactAgregadaValorFondo v
where v.idProducto = 'OBL' --and v.tipoIdAfiliado = 'CC' and v.idAfiliado IN( '70075456', '21658782', '8348515')
group by tipoIdAfiliado, idAfiliado),
Mesada AS(select sum(numValorPago) valorPago
,tipoIdAfiliado
,IdIdentificacion1
,descPeriodoPago
,ConceptoPago
,ROW_NUMBER() OVER (PARTITION BY tipoIdAfiliado, IdIdentificacion1  ORDER BY descPeriodoPago desc) RN
from BIProteccionSA.SA.StgPOCuentasIndividualesCotObliChe
group by tipoIdAfiliado, IdIdentificacion1, descPeriodoPago, ConceptoPago)
select d.tipoIdAfiliado, d.idAfiliado, d.nombreCompletoAfiliado, d.descSexo, d.dtmFechaNacimiento 
,d.idEstadoAfiliado, c.valorFondoPesos CAI, m.ConceptoPago, m.descPeriodoPago, m.valorPago
from Datos_Basicos d left outer join cai c on d.tipoIdAfiliado = c.tipoIdAfiliado and d.idAfiliado = c.idAfiliado
left outer join Mesada m on d.tipoIdAfiliado = m.tipoIdAfiliado and d.idAfiliado = m.IdIdentificacion1 and m.rn = 1;



select a.tipoIdAfiliado, a.idAfiliado, count(*)
from BIProteccionDW.APOLO.vDimAfiliados a inner join BIProteccionDW.APOLO.vAuxLineasNegocioAfiliados l
on a.skPersona = l.fkPersona and l.idLineaNegocio = 'PO' and l.idEstadoAfiliado in('PEN','RES')
group by a.tipoIdAfiliado, a.idAfiliado
having count(*) >1;