with semanas as (select * from (
select t.solbp_tipsol, t.solbp_nrosol, coalesce(DATSO_SEMCOT, 0) DATSO_SEMCOT, row_number() over (partition by  t.solbp_tipsol, cast(t.solbp_nrosol as string) order by DATSO_SECETP desc) rn
FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_DATSO t 
where DATSO_PROGEN = 'APR') d 
where d.rn = 1)
SELECT S.SOLBP_TIPSOL, s.solbp_codcaumod
,S.SOLBP_ESTADO 
,S.afi_hash64  
,TERCE_SEXO 
,'' BENEF_ESTBEN
,TERCE_FECNAC
,TERCE_CALBEN
,'' TIPBE_TIPBEN, '' SUBTI_SUBBEN
,(SELECT COALESCE(SUM(MOVCOT), 0) 
	FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_MOVARC I, notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_AFIARC J 
	WHERE i.MOVCO4 = j.AFINUM AND j.afi_hash64 = S.afi_hash64 AND i.MOVCO1='06' AND i.MOVTIP = '31' AND i.MOVEST<>'R') 
	+ (SELECT COALESCE(SUM(MOVCO2), 0) 
		FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_MOVAR1 I , notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_AFIAR3 J 
		WHERE i.MOVNU3 = j.AFINU2 AND j.afi_hash64=S.afi_hash64 AND i.MOVC11='06' AND i.MOVTI1 = '31' AND i.MOVES1<>'R' ) AS BONO
,S.SOLBP_FECSIN 
,P.PAGV01
,P.PAGFE6
,H.DATSO_SEMCOT
,S.SOLBP_NROSOL 
FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PAGAR2 P, notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_SOLBP S, 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_TERCE T, SEMANAS H,
notional-radio-302217.DatalakeAnalitica.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 has, 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_BENEF B,
afiliados-pensionados-prote.afiliados_pensionados.tabla_fed_Apolo ap,
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_TERCE_Z51 T_has
WHERE P.afi_hash64=S.afi_hash64 AND p.PAGCON IN ('22', '23') 
AND S.SOLBP_TIPSOL <>'AUF' AND SOLBP_ESTADO='CAN' AND p.pages2 NOT IN ('CAN', 'GEN', 'PPG') and S.SOLBP_TIPSOL IN ('INV', 'VEJ') 
AND has.tipoIdAfiliado=T_has.TERCE_TIPIDE       
AND has.idAfiliado = T_has.TERCE_NROIDETER  
AND T.TER_HASH64 = T_has.TER_HASH64                         
and h.solbp_tipsol = s.solbp_tipsol and h.solbp_nrosol = s.solbp_nrosol
and has.afi_hash64=p.afi_hash64 and ap.tipoIdAfiliado = has.tipoIdAfiliado and ap.idAfiliado = has.idAfiliado
union all                                                                     
SELECT S.SOLBP_TIPSOL, s.solbp_codcaumod 
,S.SOLBP_ESTADO 
,S.afi_hash64  
,TERCE_SEXO 
,BENEF_ESTBEN
,TERCE_FECNAC 
,TERCE_CALBEN
,TIPBE_TIPBEN, SUBTI_SUBBEN
,(SELECT COALESCE(SUM(MOVCOT), 0) 
	FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_MOVARC I, notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_AFIARC J 
	WHERE MOVCO4 = AFINUM AND j.afi_hash64=S.afi_hash64 AND MOVCO1='06' AND MOVTIP = '31' AND MOVEST<>'R') 
	+ (SELECT COALESCE(SUM(MOVCO2), 0) 
		FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_MOVAR1 I , notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_AFIAR3 J 
		WHERE MOVNU3 = AFINU2 AND j.afi_hash64=S.afi_hash64 AND MOVC11='06' AND MOVTI1 = '31' AND MOVES1<>'R' ) AS BONO 
,S.SOLBP_FECSIN 
,P.PAGV01
,P.PAGFE6
,H.DATSO_SEMCOT 
,S.SOLBP_NROSOL                                     
FROM notional-radio-302217.DatalakeAnalitica.AS400_FPOBLIDA_PAGAR2 P, notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_SOLBP S, 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_TERCE T, 
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_BENEF B, SEMANAS H, 
notional-radio-302217.DatalakeAnalitica.SQLSERVER_BIPROTECCIONDW_DW_DIMAFILIADOS_Z51 has,
afiliados-pensionados-prote.afiliados_pensionados.tabla_fed_Apolo ap,
notional-radio-302217.DatalakeAnalitica.AS400_FPOBYPDA_BYP_TERCE_Z51 T_has
WHERE P.afi_hash64=S.afi_hash64 AND PAGCON IN ('22', '23')  
AND S.SOLBP_TIPSOL <>'AUF' AND (S.SOLBP_NROSOL = P.PAGNIT or P.PAGNIT =0)                                                            
AND SOLBP_ESTADO='CAN'  AND pages2 NOT IN ('CAN', 'GEN', 'PPG')                                 
and   S.SOLBP_TIPSOL IN ('SBV')  AND T.TERCE_CONSECPK =B.BENEF_TERBEN 
and h.solbp_tipsol = s.solbp_tipsol and h.solbp_nrosol = s.solbp_nrosol
and has.afi_hash64=p.afi_hash64 and ap.tipoIdAfiliado = has.tipoIdAfiliado and ap.idAfiliado = has.idAfiliado
AND T.TER_HASH64 = T_has.TER_HASH64 